import { useState } from "react";

function useToggle(initialVal = false){
    const [myState, setMyState] = useState(initialVal)
    const toggle = () => {
        setMyState(!myState)
    }
    return [myState, toggle]
}

export default useToggle
