import React from 'react'
import useToggle from './hooks/useToggle'


function Toggler(){
    const [isHappy, toggleIsHappy] = useToggle(true)
    const [isSad, toggleIsSad] = useToggle(false)

    return (
        <div>
            <h1 onClick = {toggleIsHappy}>{isHappy ? "😃" : "😭"} </h1>
            <h1 onClick = {toggleIsSad}>{isSad ? "😞" : "💓"} </h1>
        </div>
    )
}

export default Toggler
