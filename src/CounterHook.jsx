import React, {useState} from 'react';

function CounterHook(){
    const [count, setCount] = useState(0)
    return (
        <>
        <div>
            <h1>the count is: {count} </h1>
            <button onClick={() => setCount(count + 1)}>Add One!</button>
        </div>
        </>
    )
}


export default CounterHook;

// usestate takes initial value of your state
// useState returns an array -
