import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import CounterHook from './CounterHook'
import Toggler from './Toggler'
import SWMovies from './SWMovies'
import './App.css'


function App() {


  return (
    <>
    <div className="App">
      <header className="App-header">
      {/* < CounterHook />
        < Toggler /> */}
        < SWMovies />
      </header>
    </div>

    </>
  )
}

export default App
